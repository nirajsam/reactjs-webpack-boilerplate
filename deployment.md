# Project Deployment
## Steps
1.  
    ```sh
    npm install -g @cloudflare/wrangler
    ```

2. ```sh
    wrangler config --api-key
    ```
    Terminal will prompt to enter email and api key, enter as mentioned below:

        email: fire.on.will@gmail.com
    
        apikey: 838e98dd62c84603c4282664daf7ebc47a0dc
        

3. edit wrangler.toml (exists on project root) and change name property to a unique poject name eg. **prasharm45-demo**, this is used to generate a unique web URL. 
    ```sh
        name = "prasharm45-demo" // change this to any unique name
    ```

4.  
    ```sh
        npm run build
   ```
   
  5.   ```sh
        wrangler publish
        ```
   
        your app will be published on a unique web url - https://prasharm45-demo.rampup.workers.dev
        
        For updated code deployment just follow step 4 and 5 
   
complete details are available on below URL - 
https://developers.cloudflare.com/workers/tutorials/deploy-a-react-app-with-create-react-app
