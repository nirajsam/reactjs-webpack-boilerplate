import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Heading } from './Heading'

describe('Heading.jsx', () => {
  test('should render heading component', () => {
    const { getByText } = render(<Heading>My App Heading</Heading>)
    expect(getByText(/My App Heading/i)).toBeInTheDocument()
  })
})
